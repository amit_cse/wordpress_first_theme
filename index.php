<?php
/**
 * The index for our theme
 *
 * This is the template that displays all of the <index> section and everything like posts in the database>
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage customtheme
 * @since 1.0
 * @version 1.0
 */

?>   
<?php get_header(); ?><!--Includes the header in index -->
	<div class="jumbotron gradient-bg margin-neg-20">
		<div class="container text-center">
		<h1>Welcome To The Website</h1>
	    </div>
	</div>
	<div class="container">
	<?php if (have_posts()) : ?> <!--returns true or false -->
		<?php while ( have_posts() ) : the_post(); ?><!--if true goes for every single post -->
			<h2><?php the_title(); ?></h2>
			<?php the_content(); ?><!--delivers the content in database -->
		<?php endwhile ?>	
	<?php endif ?>
	
	<?php get_sidebar(); ?>
	</div><!--container div -->	
<?php get_footer(); ?><!-- include the footer in index -->