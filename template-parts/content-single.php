<?php
/**
 * This is the page for displayig single post content
 *
 * This is the template that displays all of the <index> section and everything like posts in the database>
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage customtheme
 * @since 1.0
 * @version 1.0
 */

?>   
<div id="post-<?php the_ID(); ?>"<?php post_class(); ?>>
<h2><?php the_title(); ?></h2>
<p><?php the_date(); ?></p>
<?php the_content(); ?><!--delivers the content in database -->

<?php 
wp_link_pages(array(
	'before'      => '<div class="page-links"><span class="page-links-title">' .__('pages: ',              'customtheme').'</span>',
	'after'       => '</div>',
	'link_before' => '<span>',
	'link_after'  => '</span>',
	'pagelink'    => '<span class="screen-reader-text">'.__( 'page ', 'customtheme').'</ span>%',
	'separator'   => '<span class="screen-reader-text">,</span>'
		

));

 ?>
 </div>