<?php
/**
 * Template Name: Slim Page
 *
 * This is the template that displays all of the <About Us> section and everything like posts in the database>
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage customtheme
 * @since 1.0
 * @version 1.0
 */

?>   
<?php get_header(); ?><!--Includes the header in index -->
	
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-md-offset-3">
				<?php if (have_posts()) : ?> <!--returns true or false -->
					<?php while ( have_posts() ) : the_post(); ?><!--if true goes for every single post -->
						<h1 class="text-center"><?php the_title(); ?></h1>
						<?php the_content(); ?><!--delivers the content in database -->
					<?php endwhile ?>	
				<?php endif ?>
			</div>
	    </div>
	</div>	
<?php get_footer(); ?><!-- include the footer in index -->