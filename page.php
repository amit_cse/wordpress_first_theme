<?php
/**
 * This is the template for displaying page type
 *
 * This is the template that displays all of the <About Us> section and everything like posts in the database>
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage customtheme
 * @since 1.0
 * @version 1.0
 */

?>   
<?php get_header(); ?><!--Includes the header in index -->
	
	<div class="container">
	<?php if (have_posts()) : ?> <!--returns true or false -->
		<?php while ( have_posts() ) : the_post(); ?><!--if true goes for every single post -->
			<h2><?php the_title(); ?></h2>
			<?php the_content(); ?><!--delivers the content in database -->
		<?php endwhile ?>	
	<?php endif ?>
	</div>	
<?php get_footer(); ?><!-- include the footer in index -->