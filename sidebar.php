<?php
/**
 * The sidebar for our theme
 *
 * This is the template that displays all of the <index> section and everything like posts in the database>
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage customtheme
 * @since 1.0
 * @version 1.0
 */

?>   
<?php if (is_active_sidebar('sidebar-1')) : ?>
	<aside id="secondary" class="sidebar widget-area" role="complementry">
		<?php dynamic_sidebar('sidebar-1'); ?>
	</aside>
<?php endif; ?>	