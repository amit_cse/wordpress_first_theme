<?php
/**
 * The single post  for our theme
 *
 * This is the template that displays all of the <index> section and everything like posts in the database>
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage customtheme
 * @since 1.0
 * @version 1.0
 */

?>   
<?php get_header(); ?><!--Includes the header in index -->
	
	<div class="container">
		<div class="row">
			<div class="col-md-8">
				<?php if (have_posts()) : ?> <!--returns true or false -->
					<?php while ( have_posts() ) : the_post(); ?><!--if true goes for every single post -->
						<?php get_template_part('template-parts/content', 'single') ?>
						 <?php 
						    if (comments_open() || get_comments_number()) {
						    	comments_template();
						    	//grab the comments template.
						    	//always do this in the loop

						    }
						  ?>
					<?php endwhile ?>	
				<?php endif ?>
				<?php 
				   if ( is_singular( 'post' )) {
				   	the_post_navigation( array(
				   		'next_text' => 'Next',
				   		'prev_text' => 'Previous'
				   	));
				   }
				 ?> 
			</div>
			<div class="col-md-4">
			<?php get_sidebar(); ?>
			</div>
	    </div><!-- row div-->
	</div><!--container div -->	
<?php get_footer(); ?><!-- include the footer in index -->