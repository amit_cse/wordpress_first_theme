<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage customtheme
 * @since 1.0
 * @version 1.0
 */

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>><!--It setup a language in the html "en-us" -->
<head>
	<!--important head tags a/c to wordpress guidelines -->
	<meta charset="<?php bloginfo( 'charset' ); ?>" /><!--if (no idea) "check defination " -->
	<link rel="profile" href="http://gmpg.org/xfn/11" /><!--Same as above -->
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
    <?php if ( is_singular() && get_option( 'thread_comments' ) ) wp_enqueue_script( 'comment-reply' ); ?><!--#over_whelmed -->
    
    <?php wp_head(); ?><!--Responsible for loading the style -->
</head>
<body <?php body_class(); ?>>
	<nav class="navbar gradient-bg main-custom-nav">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
					<span class="sr-only">Toggle-navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#">Brand</a>        
			</div>
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav navbar-right">
					
					<?php
					wp_nav_menu( array(
						'theme_location' => 'primary',
						'menu_class'     => 'primary-menu',
						'container'      =>  false,
						'items_wrap'     => '%3$s'
					)); 
					 ?>
					
				</ul>
				
			</div>
			
		</div><!--container-fluid -->
		
	</nav>